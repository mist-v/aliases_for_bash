
if status is-interactive
    # Commands to run in interactive sessions can go here
end

## STAR THE FUNCTIONS ##
# Functions for printing a column (splits input on whitespaces)
# ex: echo 1 2 3 | coln 3
# output 3
function coln
    while read -l input
        echo $input | awk '{print $'$argv[1]'}'
    end
end

# function for printing a row
# ex: seq 3 | rown 3
# output 3
function rown --argument index
 set -n "$index -p"
end

# function for ignoring the first 'n' lines
# ex: seq 10 | skip 5
# result: prints everything but the fisrt 5 lines
function skip --argument n
    tail +(math 1 + $n)
end

# function for taking the first 'n' lines
# ex: seq 10 | take 5
# result: print only the first 5 lines
function take --argument number
    head -$number
end
## END OF THE FUNCTIONS ##

## EXPORT ##
set fish_greetings
set TERM "xterm-256color"
set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $fish_user_paths

## ALIASES ##
alias la='ls -a'
alias li='ls -li'
alias lh='ls -lhA'
alias gp='grep -i'
alias his='history clear'
alias ac='acpi -b'
alias cat='bat -n --decorations always --color auto'
alias ydp='yt-dlp -f'
alias v='vim'
alias vi='doas vim'
alias ec='echo -e'
alias clone='git clone'
alias viss='cli-visualizer'
alias free='free -m'
alias wtt='curl -4 wttr.in'


printf "60" | doas tee /sys/class/backlight/amdgpu_bl1/brightness

# Set colors in fish-shell
#set di '00;32' && eval (dircolors -b ~/.dir_colors) && dircolors -p > ~/.dir_colors

# letters

figlet ' Salom!! '

toilet -t --gay -f ascii9 'Har doim, Oh-my-zsh ! '

toilet -t -f bigascii9 'farishtalar..'

toilet -t --metal -f bigmono9 ' ¡quyoshli kun ! '
