
# Alias for ls.
alias la='ls -a'
alias li='ls -li'
alias lh='ls -lhA'

# Other Alias.
alias gp='grep -i'
alias his='history -c && history -w'
alias cat='bat --wrap auto --color always --decorations always'
alias v='vim'
alias vi='doas vim'
alias ac='acpi -b'
alias ec='echo'
alias ydp='yt-dlp -f'
alias clone='git clone'
alias viss='cli-visualizer'
alias free='free -m'
alias wtt='curl -4 wttr.in'

doas tee /sys/class/backlight/amdgpu_bl1/brightness <<< 60

# letters

figlet ' Salom!! '

toilet -t --gay -f ascii9 'Har doim, Oh-my-zsh ! '

toilet -t -f bigascii9 'farishtalar..'

toilet -t --metal -f bigmono9 ' ¡quyoshli kun ! '


# param alias
search_man() {
         man $1 | grep -- $2
}


bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'


